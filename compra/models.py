from django.contrib.auth.models import User
from django.db import models
from viewflow.models import Process, Task


class Departamento(models.Model):
    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class Solicitud(models.Model):
    usuario = models.ForeignKey(
        User, on_delete=models.DO_NOTHING, related_name="usuario_dueno"
    )
    solicitante = models.ForeignKey(
        User, on_delete=models.DO_NOTHING, related_name="usuario_solicitante"
    )
    departamento = models.ForeignKey(Departamento, on_delete=models.DO_NOTHING)
    fecha = models.DateField()
    comentario = models.TextField()
    articulo = models.CharField(max_length=100)
    cantidad = models.IntegerField()
    approved = models.BooleanField(("Approved"), default=False)
    cotizacion = models.FileField(upload_to="cotizaciones/")

    class Meta:
        verbose_name = "Solicitud"
        verbose_name_plural = "Solicitudes"

    def __str__(self):
        return f"Solicitud # {self.id}"


# class SolicitudArticulos(models.Model):
#     solicitud = models.ForeignKey(
#         Solicitud, on_delete=models.DO_NOTHING, related_name="articulos"
#     )
#     articulo = models.CharField(max_length=100)
#     cantidad = models.IntegerField()

#     def __str__(self):
#         return f"Solicitud # {self.solicitud.id} - {self.articulo}"


class SolicitudProcess(Process):
    solicitud = models.ForeignKey(
        Solicitud, blank=True, null=True, on_delete=models.DO_NOTHING
    )

    class Meta:
        verbose_name = "Solicitud - proceso"
        verbose_name_plural = "Solicitud - procesos"
