import os

from django.utils.translation import ugettext_lazy as _
from viewflow import flow, frontend, lock
from viewflow.base import Flow, this
from viewflow.flow import views as flow_views

from .models import SolicitudProcess
from .views import SolicitudView, StartView


@frontend.register
class SolicitudFlow(Flow):
    """
    Nueva compra
    Proceso para generar nueva solicitud de compra
    """

    process_class = SolicitudProcess
    process_title = "Compra"
    process_description = "Proceso para generar nueva solicitud de compra."

    lock_impl = lock.select_for_update_lock

    # summary_template = _("'{{ process.fecha }}' message to the world")

    solicitud = (
        flow.Start(StartView, task_title="Nueva compra")
        .Permission(auto_create=True)
        .Next(this.aprobar)
    )

    aprobar = (
        flow.View(SolicitudView, fields=["approved"], task_title=_("Approve"))
        .Permission(auto_create=True)
        .Assign(username="avasquez")
        .Next(this.adjuntar)
    )

    adjuntar = (
        flow.View(
            SolicitudView,
            fields=["cotizacion"],
            task_title="adjuntar cotizacion",
        )
        .Permission(auto_create=True)
        .Next(this.end)
    )

    # send = flow.Handler(
    #     this.send_hello_world_request, task_title=_("Send message")
    # ).Next(this.end)

    end = flow.End(task_title=("Final del proceso"))

    def send_hello_world_request(self, activation):
        with open(os.devnull, "w") as world:
            world.write(activation.process.text)
