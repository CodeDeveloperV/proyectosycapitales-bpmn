from django.apps import AppConfig


class CompraConfig(AppConfig):
    icon = '<i class="material-icons">flag</i>'
    name = "compra"
