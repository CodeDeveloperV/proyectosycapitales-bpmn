from django.contrib import admin
from viewflow.admin import ProcessAdmin

from . import models

# class SolicitudArticulosInline(admin.StackedInline):
#     model = models.SolicitudArticulos
#     extra = 2


class SolicitudProcessAdmin(ProcessAdmin):
    icon = '<i class="material-icons">flag</i>'
    list_display = ["pk", "created", "status", "participants"]
    list_display_links = ["pk", "created"]


@admin.register(models.Solicitud)
class SolicitudAdmin(admin.ModelAdmin):
    # inlines = [SolicitudArticulosInline]
    pass


admin.site.register(models.SolicitudProcess, SolicitudProcessAdmin)
admin.site.register(models.Departamento)
