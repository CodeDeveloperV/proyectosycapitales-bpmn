from django import forms

from .models import Solicitud

# SolicitudForm = forms.modelform_factory(
#     Solicitud,
#     fields=("usuario", "solicitante", "departamento", "fecha", "comentario"),
# )


class SolicitudForm(forms.ModelForm):
    """Form definition for Solicitud."""

    class Meta:
        """Meta definition for Solicitudform."""

        model = Solicitud
        fields = (
            "usuario",
            "solicitante",
            "departamento",
            "fecha",
            "articulo",
            "cantidad",
            "comentario",
        )
