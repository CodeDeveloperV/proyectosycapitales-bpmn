from django.shortcuts import render
from django.views import generic
from material import Fieldset, Layout, Row
from viewflow.flow.views import FlowMixin, StartFlowMixin

from .forms import SolicitudForm


class StartView(StartFlowMixin, generic.UpdateView):
    form_class = SolicitudForm
    layout = Layout(
        Fieldset(
            "Detalles de la solicitud",
            Row("usuario", "solicitante"),
            Row("departamento", "fecha"),
            Row("comentario"),
        ),
        Fieldset("Articulos solicitados", Row("articulo", "cantidad")),
    )

    def get_object(self):
        return self.activation.process.solicitud

    def activation_done(self, form):
        solicitud = form.save()
        self.activation.process.solicitud = solicitud
        super(StartView, self).activation_done(form)


class SolicitudView(FlowMixin, generic.UpdateView):
    def get_object(self):
        return self.activation.process.solicitud
